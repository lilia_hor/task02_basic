package com.epam;

import java.util.Scanner;

/**
 * The Fibonacci program builds Fibonacci numbers.
 */
class Fibonacci {
    /**
     * The method prints the biggest odd number and the biggest even number.
     * This returns percentage of even numbers and of odd numbers.
     */
    public static void createFib() {
        System.out.println("Enter size of set (F)");
        Scanner sizeSetScan = new Scanner(System.in);
        int sizeSet = sizeSetScan.nextInt();
        int[] fib = new int[sizeSet];
        int i = 0;
        while (i < sizeSet) {
            fib[i] = (i < 2) ? 1 : fib[i - 2] + fib[i - 1];
            i++;
        }

        int f1 = fib[0];
        for (i = 0; i < sizeSet; i++) {
            if ((fib[i] > f1) && (fib[i] % 2 != 0)) {
                f1 = fib[i];
            }
        }
        System.out.println(f1 + " -the biggest odd number");
        int f2 = fib[0];
        for (i = 0; i < sizeSet; i++) {
            if ((fib[i] > f2) && (fib[i] % 2 == 0)) {
                f2 = fib[i];
            }
        }
        System.out.println(f2 + " -the biggest even number");
        int countOdd = 0;
        int countEven = 0;
        for (i = 0; i < sizeSet; i++) {
            if (fib[i] % 2 == 0) {
                countOdd += 1;
            } else {
                countEven += 1;
            }
        }
        double percentOfOdd;
        double percentOfEven;
        percentOfOdd = (double) countOdd / (countOdd + countEven) * 100;
        percentOfEven = (double) countEven / (countOdd + countEven) * 100;
        System.out.println(percentOfEven + "- percentage of even numbers");
        System.out.println(percentOfOdd + "- percentage of odd numbers");
    }
}
