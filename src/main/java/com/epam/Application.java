package com.epam;

import java.util.Scanner;

/**
 * The Application program creates a growing sequence of even numbers
 * and a falling sequence of odd number.
 */
public class Application {
    /**
     * The method uses the interval that entered user.
     * This prints  odd numbers from start to the end of interval and even from end to start.
     * This prints the sum of odd and even numbers.
     */
    public static void generateNumber() {
        System.out.println("Enter  numbers from the range [1;100] ");
        Scanner firstScan = new Scanner(System.in);
        int firstNumber = firstScan.nextInt();
        Scanner lastScan = new Scanner(System.in);
        int lastNumber = lastScan.nextInt();
        int countNumber = lastNumber - firstNumber + 1;
        int[] arr = new int[countNumber];
        for (int i = 0; i < countNumber; i++) {
            arr[i] = firstNumber + i;
        }
        for (int i = 0; i < countNumber; i++) {
            if (arr[i] % 2 == 0) {
                System.out.print(arr[i] + " ");
            }
        }
        System.out.println("");
        for (int i = countNumber - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int min = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = min;
                }
            }
            if ((arr[i] % 2 == 1)||(arr[i] % 2 == -1)) {
                System.out.print(arr[i] + " ");
            }
        }
        int sumOdd = 0;
        for (int i = 0; i < countNumber; i++) {
            if ((arr[i] % 2 == 0)||arr[i] % 2 == -1) {
                sumOdd += arr[i];
            }
        }
        System.out.println("");
        System.out.println(sumOdd + " -the sum of odd number");
        int sumEven = 0;
        for (int i = 0; i < countNumber; i++) {
            if (arr[i] % 2 != 0) {
                sumEven += arr[i];
            }
        }
        System.out.println(sumEven + " -the sum of even number");
    }
}
