package com.epam;

import java.util.Arrays;
import java.util.Scanner;

/**
 * This is the main method which makes use of generationNumber and createFib methods.
 *
 * @version 1.0
 * @since 12-11-2019
 */
public class Main {
    public static void main(String[] args) {
        Application.generateNumber();
        Fibonacci.createFib();
    }
}
